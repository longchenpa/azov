\babel@toc {russian}{}\relax 
\contentsline {section}{\numberline {1}Навіщо банити російську культуру}{5}{}%
\contentsline {section}{\numberline {2}Архітектура російського фашизму}{6}{}%
\contentsline {subsection}{\numberline {2.1}Теорія}{6}{}%
\contentsline {subsection}{\numberline {2.2}Нова етика}{10}{}%
\contentsline {section}{\numberline {3}Глобальна культурна денонсація}{13}{}%
\contentsline {section}{\numberline {4}Сад націй}{15}{}%
\contentsline {subsection}{\numberline {4.1}Концепція просвітленого царства}{15}{}%
\contentsline {section}{\numberline {5}Я Собака Азову}{19}{}%
\contentsline {section}{\numberline {6}Свічечка Букви Ї}{21}{}%
\contentsline {section}{\numberline {7}Гнів Українців}{22}{}%
\newpage 
\contentsline {section}{\numberline {8}Мета Держави}{24}{}%
\contentsline {subsection}{\numberline {8.1}Неправильне розуміння освіти}{24}{}%
\contentsline {subsection}{\numberline {8.2}Освіта як мета держави}{25}{}%
\contentsline {subsection}{\numberline {8.3}Інститут формальної математики та філософії}{26}{}%
\contentsline {subsection}{\numberline {8.4}Інститут формальної літератури}{26}{}%
\contentsline {subsection}{\numberline {8.5}Інститут формального мистецтва}{26}{}%
\contentsline {subsection}{\numberline {8.6}Корінь нації}{28}{}%
\contentsline {section}{\numberline {9}Прокльони}{30}{}%
\contentsline {section}{\numberline {10}Тільки одне питання}{32}{}%
\contentsline {section}{\numberline {11}Остання лекція}{33}{}%
\contentsline {subsection}{\numberline {11.1}Планета мислячих істот є сад націй}{33}{}%
\contentsline {subsection}{\numberline {11.2}Пацифізм є запрошенням до свого знищення}{34}{}%
\contentsline {subsection}{\numberline {11.3}Мислення є ненависть до ігнорування реальності}{36}{}%
\contentsline {subsection}{\numberline {11.4}Дорогоцінність життя є практикою благородних}{37}{}%
\contentsline {subsection}{\numberline {11.5}Світогляди та ідеології є коіндуктивні стріми}{40}{}%
\contentsline {subsection}{\numberline {11.6}Закон причини та наслідку є безпомилковим}{45}{}%
\contentsline {section}{\numberline {12}Малороси}{50}{}%
\contentsline {section}{\numberline {13}Ти особисто винен}{51}{}%
